# A BuildStream project generator
Generate a BuildStream project containing an arbitrary number of dummy import
elements with either simple or 'fan'-like depencency relationships.

## Overview
`bstgen` has two commands: `simple` and `fan`.

### Simple
This creates a project containing a set of elements that are arranged into a
specified number of levels (specified by the option `--project-levels`) of elements.
Each level has a width (specified by the option `--project-widths`) which defines
the number of elements in that level. In each level, every element depends on every
element in the level beneath.

This can create projects that contain elements with very large numbers of
dependencies, which can be hard for bst to process, thus they are ideal for benchmarking.
It should be noted that such project's are unrepresentative of real projects.

### Fan
This creates a project which starts with one element and then creates a specified
number of levels of elements (specified by the option `--fan-levels`). The number
of elements in each level is one more than the previous level. For each level, every
element depends on 2 elements from the level below (unless it is the first or last element
in the level, in which case, it depends on the first or element from the level below,
respectively).

Projects created by the `fan` command are more realistic than those created by
the `simple` command although it should be noted that they are still a lot less
complex than real projects. However, they do seem to expose the same bottlenecks
and thus *might* be useful for general performance investigations.

## Example
### Generating a 'fan' project
To generate a buildstream project with 6 elements with fan style connections:

```
bstgen fan --fan-levels 3 target_directory
```

Once done, `target_directory` will contain:
```
target_directory:
    - elements:
        - build_all.bst
        - project_0.bst
        - project_1.bst
        - project_2.bst
        - project_3.bst
        - project_4.bst
        - project_5.bst
    - files:
        <files for the project's content>
    - project.conf
```

project_0.bst represents the first element in the chain (the base element),
that project_1.bst and project_2.bst depend on. project_3.bst will depend on
project_1.bst and project_5.bst will depend on project_2.bst but project_4.bst
will depend on both project_1.bst and project_2.bst.

The advantage of using the fan style project over the simple style is that
you can create projects with large numbers of element that do not have elements
with large numbers of dependencies and that do not have too deep dependency
graphs.

### Building the generated projects

To build the generated projects:

```bash
cd <output directory>
bst build build_all.bst
```

If you wish to make sure that you can rebuild the project many times it may be
wise to run the project with a custom cache you can easily clear.

```
export XDG_CACHE_HOME=/abs/path/to/<output_directory>/tmpcache; bst build build_all.bst
```
