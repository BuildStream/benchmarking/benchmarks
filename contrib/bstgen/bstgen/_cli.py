#!/usr/bin/env python3

#  Copyright Bloomberg Finance LP
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library. If not, see <http://www.gnu.org/licenses/>.
#
#  Authors:
#        Antoine Wacheux <awacheux@bloomberg.net>
#        William Salmon <will.salmon@codethink.co.uk>

import click
import os
from os import path
from . import bstgen_simple, bstgen_fan


@click.group(context_settings=dict(help_option_names=['-h', '--help']))
def cli(**kwargs):
    pass

@cli.command(short_help="Simple projects with every element per level depending on all elements in the previous level")
@click.option("-n", "--num-file", type=int, help="number of files per project", default=1)
@click.option("-f", "--file-size", type=int, help="size of project files in bytes", default=1)
@click.option("-w", "--project-width", type=int, help="number of elements per level of the project", default=1)
@click.option("-l", "--project-levels", type=int, help="number of levels of elements that depend on the previous" + \
        "level in the project", default=1)
@click.option("-s", "--shape", type=click.Choice(['square', 'grow']), help="how the number of elements per level" + \
        "changes, if square every level has the same number of elements",
        default="square")
@click.argument('output_directory')
def simple(num_file, file_size, project_width, project_levels, shape, output_directory):
    output_directory = path.abspath(output_directory)

    bstgen_simple(output_directory, num_file, file_size, project_levels, project_width, shape)


@cli.command(short_help="More complex projects with each element depending on one or two elements from the" + \
        "previous level")
@click.option("-n", "--num-file", type=int, help="number of files per project", default=1)
@click.option("-f", "--file-size", type=int, help="size of project files in bytes", default=1)
@click.option("-l", "--fan-levels", type=int, help="number of levels in a fan_numbers", default=1)
@click.option("-r", "--fan-numbers", type=int, help="the number of fan restarts per project", default=1)
@click.argument('output_directory')
def fan(num_file, file_size, fan_levels, fan_numbers, output_directory):
    output_directory = path.abspath(output_directory)

    bstgen_fan(output_directory, num_file, file_size, fan_levels, fan_numbers)
