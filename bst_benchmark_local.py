#!/usr/bin/env python3

#
#  Copyright (C) 2019 Codethink Limited
#  Copyright (C) 2019 Bloomberg Finance LP
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library. If not, see <http://www.gnu.org/licenses/>.
#
#  Authors:
#        Lachlan Mackenzie <lachlan.mackenzie@codethink.co.uk>

import tempfile
import argparse
import os
import logging
from shutil import copyfile
import sys

import git

import bst_benchmarks.main
import generate_benchmark_config
import digest_results

# This command line executable acts as a method to generate a locally run
# benchmark CI run. A number of elements are passed in to check that
# configuration settings are tenable - path to repository, buildstream branch,
# SHAs to be tested (defaults to HEAD of branch if not specified), specific tests
# to be carried out (defaults to default.benchmark if not set), output file
# (defaults to stdout if not set), whether master should be tested as a comparable
# (default is yes)
#
# output_file - the path where the bespoke benchmark file is to be placed
# repo_path - path to the local buildstream repo.
# bs_branch - the branch of buildstream being considered.
# shas - shas to be tested.


def main():
   # Define all the defaults explicitly
   repo_path = 'https://gitlab.com/BuildStream/buildstream.git'
   bs_branch = 'master'
   shas_to_be_tested = []
   docker_tag = "30-latest"
   docker_image = 'registry.gitlab.com/buildstream/buildstream-docker-images/testsuite-fedora'
   tests_to_run = []
   debug = False
   keep_results = False
   keep_path = "results.json"

   def make_help(message):
      return message + " (Default: %(default)s)"

   # Resolve commandline arguments
   parser = argparse.ArgumentParser(description="Automatically set up test environments for a set "
                                    "of buildstream benchmarks and run them locally using docker.")
   parser.add_argument("tests_to_run",
                       help=make_help("The benchmark tests to run"),
                       nargs="*",
                       default=["bst_benchmarks/default.benchmark"])
   parser.add_argument("-o", "--output_file",
                       help=make_help("The file to write benchmark results to"),
                       type=argparse.FileType("w"),
                       default="-")
   parser.add_argument("-r", "--repo_path",
                       help=make_help("The repository from which to clone branches to compare "
                                      "against the defaults set in the test. Note that this can"
                                      " be a local path using file://"),
                       default=repo_path,
                       type=str)
   parser.add_argument("-b", "--bs_branch",
                       help=make_help("The branch to clone from the set repository"),
                       default=bs_branch,
                       type=str)
   parser.add_argument("-s", "--shas_to_be_tested",
                       help=make_help("SHAs to clone from the set repository"),
                       action='append')
   parser.add_argument("-d", "--docker_tag",
                       help=make_help("The tag to use for the buildstream image"),
                       default=docker_tag,
                       type=str)
   parser.add_argument("-p", "--docker_image",
                       help=make_help("The docker image to use"),
                       default=docker_image,
                       type=str)
   parser.add_argument("-g", "--debug",
                       help=make_help("Show debug messages"),
                       default=debug,
                       action='store_true')
   parser.add_argument("-k", "--keep_results",
                       help=make_help("Retain raw benchmarking results"),
                       default=keep_results,
                       action='store_true')
   parser.add_argument('--results-file',
                       help=make_help("The file to store benchmarking results in; Implies"
                                      " --keep-results"),
                       default=keep_path,
                       type=str)
   args = parser.parse_args()

   if bool(args.output_file):
      output_file = args.output_file

   if bool(args.repo_path):
      repo_path = args.repo_path

   if bool(args.bs_branch):
      bs_branch = args.bs_branch

   if bool(args.shas_to_be_tested):
      shas_to_be_tested = args.shas_to_be_tested

   if bool(args.docker_tag):
      docker_tag = args.docker_tag

   if bool(args.tests_to_run):
      tests_to_run = args.tests_to_run

   if bool(args.debug):
      debug = True
      logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)
   else:
      logging.basicConfig(stream=sys.stderr, level=logging.INFO)

   if bool(args.keep_results):
      keep_results = args.keep_results

   if bool(args.results_file):
      keep_path = os.path.abspath(args.results_file)
      keep_results = True

   commits = list()

   # Create a temporary directory for all work
   with tempfile.TemporaryDirectory(prefix='temp_staging_location') as temp_staging_area:
      # Get a reference to the requested repository cloning if remote
      try:
         if os.path.exists(repo_path):
            logging.info("Repo path resolves locally: %s", repo_path)
            repo = git.Repo.init(repo_path, bare=False)
         else:
            logging.info("Repo path resolves remotely: %s", repo_path)
            repo = git.Repo.clone_from(repo_path, temp_staging_area)
      except git.exc.GitError as err:  # pylint: disable=no-member
         logging.error("Unable to access git repository: %s", err)
         sys.exit(1)

      # Iterate the commits in the requested branch and add to list to be
      # processed if they are as per the command line selection. Keep a
      # list of all SHAs that have been found
      shas_found = []
      try:
         repo.git.checkout(bs_branch)
         for commit in repo.iter_commits():
            if commit.hexsha in shas_to_be_tested:
               commits.append(commit)
               shas_found.append(commit.hexsha)
      except git.exc.GitCommandError as err:  # pylint: disable=no-member
         logging.error("Could not find commits in repository '%s' for branch '%s':\n%s",
                       repo_path, bs_branch, err)
         sys.exit(1)

      # Check list of found SHAs against original list and flag any missing
      shas_not_found = [sha for sha in shas_to_be_tested if sha not in shas_found]
      if shas_not_found:
         logging.error("SHA(s) could not be found: %s", shas_not_found)
         sys.exit(1)

      # Create a temporary file reference for the benchmark versioning configuration
      # file that will be processed together with the selected benchmark test(s).
      output_tmp_file = os.path.join(temp_staging_area, 'output_file.benchmark')

      # Generate the bechmark versioning configuration file for selected parameters
      try:
         generate_benchmark_config.generate_benchmark_configuration(
            output_file=output_tmp_file,
            list_of_shas=commits,
            docker_version=docker_tag,
            bs_branch=bs_branch,
            bs_path=repo_path,
            docker_path=docker_image)
      # pylint: disable=broad-except
      except Exception as err:
         logging.error("Creating benchmarking configuration failed:\n%s", err)
         sys.exit(1)

      test_set = []
      # Add tests to run
      for test in tests_to_run:
         if test in test_set:
            logging.error("Duplicate benchmarking test will be ignored: %s", test)
         else:
            test_set.append(test)

      # Generate the commandline parameters for the benchmarking versioning together
      # with the selected benchmarking test(s).
      test_set.append(output_tmp_file)

      results_tmp_file = os.path.join(temp_staging_area, 'tmp_result')

      try:
         bst_benchmarks.main.run(config_files=test_set, _debug=True,
                                 keep_images=False, reuse_images=False,
                                 output_file=results_tmp_file)
      # pylint: disable=broad-except
      except Exception as err:
         logging.error("Benchmarking failed:\n%s", err)
         sys.exit(1)

      # Copy results to keep path if set
      if keep_results:
         os.makedirs(os.path.dirname(keep_path), exist_ok=True)
         copyfile(results_tmp_file, keep_path)

      # Create temporary file for results digest
      tmp_output = os.path.join(temp_staging_area, 'tmp_result')

      # Create temporary file for results digest
      tmp_error = os.path.join(temp_staging_area, 'tmp_error')

      # TODO: _error_file is not actually in use
      digest_results.parse(files=[results_tmp_file], output_file=tmp_output, _error_file=tmp_error)

      # Write output to requested outfile
      with open(tmp_output, "r") as fin:
         output_file.write(fin.read())
      output_file.close()


if __name__ == "__main__":
   main()
