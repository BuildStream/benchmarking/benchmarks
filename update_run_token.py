#!/usr/bin/env python3

#
#  Copyright (C) 2018 Codethink Limited
#  Copyright (C) 2018 Bloomberg Finance LP
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library. If not, see <http://www.gnu.org/licenses/>.
#
#  Authors:
#        Lachlan Mackenzie <lachlan.mackenzie@codethink.co.uk>

import argparse
import os
import logging
import ntpath
import tempfile
import shutil
import operator
import datetime

from distutils.file_util import copy_file

import yaml

import token_file_processing

# This command line executable allows the updating of a given run token file
# given a path to a set of results. The token file is processed but not
# not validated (on the basis that this has already had to be done to get to
# this point). The path for the generated results files are checked and the
# most recent file established. The new version of the token file is updated
# and copied over the original
#
# token_file - the path to the token file to be updated
# input_path - the path to the cached results files
#


def main():
   token_file = 'run_token.yml'
   results_files = 'results_cache/'

   parser = argparse.ArgumentParser()
   parser.add_argument("-t", "--token_file",
                       help="The token file that is to be updated.",
                       type=str)
   parser.add_argument("-i", "--input_path",
                       help="Path for generated results",
                       type=str)
   args = parser.parse_args()

   if bool(args.token_file):
      token_file = args.token_file

   if bool(args.input_path):
      results_files = args.input_path

   print(token_file)
   print(results_files)

   if not update_run_token(token_file=token_file, results_files=results_files):
      logging.error('Unable to update token file: %s', token_file)
      os.sys.exit(1)


def update_run_token(token_file, results_files):

   # Check if token file exists and try to load
   if os.path.isfile(token_file):
      try:
         token_values = token_file_processing.process_token_file(token_file)
      except OSError as err:
         logging.error("Unable to access token file: %s", token_file)
         return False
      except yaml.YAMLError:
         logging.error("Unable to parse token file: %s", token_file)
         return False
   else:
      logging.error("Token file does not exist: %s", token_file)
      return False

   # Check if results exist and get the last generated one
   if os.path.isdir(results_files):
      file_times = {}
      print(results_files)
      for file in os.listdir(results_files):
         if file.endswith('.json'):
            file_times[os.path.join(results_files, file)] = datetime.datetime.strptime(
               os.path.basename(file), 'results-%Y-%m-%d-%H:%M:%S.json')
      if file_times:
         latest_file = max(file_times.items(), key=operator.itemgetter(1))[0]
         archive_result = ntpath.basename(latest_file)
      else:
         logging.info('No results files found from: %s', results_files)
         return False
   else:
      logging.error('Results path does not exist, nothing to update: %s', results_files)
      return False

   token_values['build']['result'] = archive_result

   buildstream_sha = os.environ.get('BUILDSTREAM_HEAD')
   if not buildstream_sha:
      logging.error('No Buildstream SHA was set')
      return False

   temp_staging_area = tempfile.mkdtemp(prefix='temp_staging_location')
   temp_file = os.path.join(temp_staging_area, 'token_temp.yaml')
   try:
      # Generate the token file.;
      token_file_processing.generate_token_file(
         temp_file,
         buildstream_sha,
         token_values['build']['bs_branch'],
         archive_result)
      # Copy the temporary token file to replace requested
      copy_file(temp_file, token_file)

   # TODO: Sort out what this error here actually means
   # pylint: disable=broad-except
   except Exception as err:
      logging.error('Unable to write token file: %s', err)
      return False
   finally:
      shutil.rmtree(temp_staging_area)

   return True


if __name__ == "__main__":
   main()
