#
#  Copyright (C) 2018 Codethink Limited
#  Copyright (C) 2018 Bloomberg Finance LP
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library. If not, see <http://www.gnu.org/licenses/>.
#
#  Authors:
#        Sam Thursfield <sam.thursfield@codethink.co.uk>
#        Jim MacArthur  <jim.macarthur@codethink.code.uk>
#        Lachlan Mackenzie <lachlan.mackenzie@codethink.co.uk>

# buildstream benchmark script

import logging
import sys

import click

import bst_benchmarks.main


@click.command()
@click.option('config_files', '--config-file', '-c', type=click.Path(),
              help="YAML description of tests to run, overriding the defaults.",
              multiple=True)
@click.option('--debug/--no-debug', default=False)
@click.option('--keep-images/--no-keep-images', default=False,
              help="Do not delete the Docker images that we create.")
@click.option('--reuse-images/--no-reuse-images', default=False,
              help="Reuse test images from previous runs if found. Beware that "
                   "changes in remote refs will not be picked up if images are "
                   "reused. (Implies --keep-images)")
@click.option('output_file', '--output-file', '-o', type=click.Path(),
              help="Output file definition", default=None)
def run(config_files, debug, keep_images, reuse_images, output_file):
   if debug:
      logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)
   else:
      logging.basicConfig(stream=sys.stderr, level=logging.INFO)
   bst_benchmarks.main.run(config_files, debug, keep_images, reuse_images, output_file)


try:
   # pylint: disable=unexpected-keyword-arg,no-value-for-parameter
   run(prog_name="benchmark")
except RuntimeError as e:
   sys.stderr.write("{}\n".format(e))
   sys.exit(1)
